#pragma once

#include "PDCurses-master\curses.h"
#include <iostream>
#include <string>
#include <Windows.h>
#include <ctime>

#pragma comment(lib,"PDCurses-master/win32a/pdcurses.lib")

//Structura opisuj�ca zmiene gracza.
struct player
{
	std::string name = "";
	int points = 0;
	int posX = 0, posY = 0;
	//Wy�wietlenie pletki gracza na podanych wsp�rz�dnych.
	auto drawPallet() -> void
	{
		mvaddch(posY - 1, posX, ACS_UARROW);
		mvaddch(posY, posX, ACS_VLINE);
		mvaddch(posY + 1, posX, ACS_DARROW);
		
	};
};
//Structura opisuj�ca zmiene pi�ki.
struct ball
{
	int posX = 0, posY = 0;
	int direction = 0; // kierunek pi�ki.
	//Rysowanie pi�ki.
	auto drawBall() -> void
	{
		mvaddch(posY, posX, ACS_DIAMOND);

	};
};

void MainMenu(); //Menu g��wne.
void DrawBoard(); //Rysowanie planszy.
void MultiplayerGame(); //Tryb gry mmi�dzy graczammi.