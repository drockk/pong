#include "utils.h"

int x, y; //Ilo�� kolumn i wierszy konsoli.

void MainMenu()
{
	const char* textMenu[] = { "Pong", "Pojedynek", "Wyj�cie" };
	int tabSize = sizeof textMenu / sizeof *textMenu; // Obliczenie rozmiaru tablicy.
	int col = 2;

	getmaxyx(stdscr, y, x); //Pobranie ilo�ci kolumn i wierszy konsoli.

	mvprintw(0, (x / 2) - (sizeof(textMenu[0]) / 2), textMenu[0]); // Wypisanie tekstu na �rodku ekranu.

	bool exit = false;
	int cursor = 1;
	do
	{
		//Wypisanie pozycji menu.
		for (int i = 1; i < tabSize; i++)
		{
			if (i == cursor) attron(A_STANDOUT);
			else attroff(A_STANDOUT);
			mvprintw(col, (x / 2) - (sizeof(textMenu[i]) / 2), textMenu[i]);
			col++;
		}
		//Zmiana pozycji kursora.
		int ch = getch();
		switch (ch)
		{
		case KEY_UP:
			cursor--;
			if (cursor < 1)
			{
				cursor = tabSize - 1;
			}
			break;
		case KEY_DOWN:
			cursor++;
			if (cursor > tabSize - 1)
			{
				cursor = 1;
			}
			break;
		case 10:
			exit = true;
			break;
		}

		clear();
		attroff(A_STANDOUT);
		mvprintw(0, (x / 2) - (sizeof(textMenu[0]) / 2), textMenu[0]);
		col = 2;
	} while (!exit);

	switch (cursor)
	{
	case 1:
		MultiplayerGame();
		break;
	case 2:
		break;
	}

}
//Rysowanie planszy
void DrawBoard()
{

	mvprintw(0, (x / 2) - (sizeof("|") / 2), "|");
	for (int i = 0; i < y; i++)
	{
		if (i == 1 || i == y - 1)
		{
			for (int j = 0; j < x; j++)
			{
				mvprintw(i, j, "#");
			}
		}
		else if (i > 1 && i < y - 1)
		{
			mvprintw(i, 0, "$");
			mvprintw(i, x - 1, "$");
			mvprintw(i, (x / 2) - 1, "|");
		}
	}
}

void MultiplayerGame()
{
	player p1, p2;
	ball ba;
	//Wprowadzenie nazw graczy.
	auto playerName = []() 
	{ 
		char temp[10];
		curs_set(1);
		refresh();
		getstr(temp);
		std::string tempString(temp);
		curs_set(0);
		return tempString;
	};

	clear(); // Czysczenie ekranu.
	//Ustawienie zmienych dla graczy.
	printw("Gracz 1 podaj imie:");
	p1.name = playerName();
	p1.points = 0;
	p1.posY = 10;
	p1.posX = 3;

	printw("Gracz 2 podaj imie:");
	p2.name = playerName();
	p2.points = 0;
	p2.posY = 10;
	p2.posX = x - 4;
	//Ustawienie zmiennych pi�ki
	ba.posX = x / 2;
	ba.posY = y / 2;
	ba.direction = rand() % 2;

	noecho();

	bool exit = false;
	do
	{
		clear(); // Czyszczenie ekranu

		DrawBoard();

		mvaddstr(0, 1, const_cast<char *>(p1.name.c_str()));
		mvaddstr(0, x - (sizeof(p2.name.c_str()) * 2), const_cast<char *>(p2.name.c_str()));

		mvprintw(0, (x / 2) - 3, "%i", p1.points);
		mvprintw(0, (x / 2) + 1, "%i", p2.points);

		p1.drawPallet();
		p2.drawPallet();

		ba.drawBall();

			//Lewo
			if (ba.direction == 0)
			{
				ba.posX--;
			}
			//Prawo
			else if (ba.direction == 1)
			{
				ba.posX++;
			}
			//G�ra lewo
			else if (ba.direction == 2)
			{
				ba.posY--;
				ba.posX--;
			}
			//G�ra prawo
			else if (ba.direction == 3)
			{
				ba.posY--;
				ba.posX++;
			}
			//D� lewo
			else if (ba.direction == 4)
			{
				ba.posY++;
				ba.posX--;
			}
			//D� prawo
			else if (ba.direction == 5)
			{
				ba.posY++;
				ba.posX++;
			}

		nodelay(stdscr, TRUE);
		int key = getch();
		switch (key)
		{
		case 119:
			{
				if (p1.posY + 1 > 4)
				{
					p1.posY--;
				}
				break;
			}
			case 115:
			{
				if (p1.posY - 1 < y - 4)
				{
					p1.posY++;
				}
				break;
			}
			case 259:
			{
				if (p2.posY + 1 > 4)
				{
					p2.posY--;
				}
				break;
			}
			case 258:
			{
				if (p2.posY - 1 < y - 4)
				{
					p2.posY++;
				}
				break;
			}
			case 27:
			{
				exit = true;
				break;
			}
		}
		if (ba.posX == 0)
		{
			ba.posX = x / 2;
			ba.posY = y / 2;
			ba.direction = rand() % 2;
			p2.points++;

		}
		if (ba.posX == x)
		{
			ba.posX = x / 2;
			ba.posY = y / 2;
			ba.direction = rand() % 2;
			p1.points++;

		}
		if (ba.posY == y - 1)
		{
			if (ba.direction == 4)
			{
				ba.direction = 2;
			}
			if (ba.direction == 5)
			{
				ba.direction = 3;
			}
		}
		if (ba.posY == 2)
		{
			if (ba.direction == 2)
			{
				ba.direction = 4;
			}
			if (ba.direction == 3)
			{
				ba.direction = 5;
			}
		}
		if((ba.posX == p1.posX) && ((ba.posY == p1.posY) || (ba.posY == (p1.posY + 1)) || (ba.posY == (p1.posY - 1))))
		{
			if (ba.direction == 0)
			{
				int f = rand() % 3;
				switch (f)
				{
				case 0:
					ba.direction = 1;
				break;
				case 1:
					ba.direction = 3;
				break;
				case 2:
					ba.direction = 5;
				break;
				}
			}
			if (ba.direction == 2)
			{
				ba.direction = 3;
			}
			if (ba.direction == 4)
			{
				ba.direction = 5;
			}
		}
		if ((ba.posX == p2.posX) && ((ba.posY == p2.posY) || (ba.posY == (p2.posY + 1)) || (ba.posY == (p2.posY - 1))))
		{
			if (ba.direction == 1)
			{
				int f = rand() % 3;
				switch (f)
				{
				case 0:
					ba.direction = 0;
					break;
				case 1:
					ba.direction = 2;
					break;
				case 2:
					ba.direction = 4;
					break;
				}
			}
			if (ba.direction == 3)
			{
				ba.direction = 2;
			}
			if (ba.direction == 5)
			{
				ba.direction = 4;
			}
		}
		if (p1.points == 10)
		{
			exit = true;
		}
		if (p2.points == 10)
		{
			exit = true;
		}
		Sleep(100);
		refresh();
	} while (!exit);
}