///Pong
///Creted by: Bartosz Jakub Zielonka

#include "utils.h"
#include <locale.h>

int main()
{
	setlocale(LC_ALL, ""); //Ustawienie obs�ugi j�zyka polskiego.
	srand(time(NULL)); //Uruchomienie maszyny liczb pseudolosowych.
	initscr(); //Uruchomienie PDCrusers.
	curs_set(0);//Ukrycie kursora.
	keypad(stdscr, TRUE);//Przechwytywanie klawiatury przez PDCurses.
	MainMenu();
	endwin(); //Wy��czenie PDCrusers.
	return 0;
}